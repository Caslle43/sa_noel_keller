const express = require("express");
const axios = require("axios");
const app = express();
const PORT = 3001;

app.use(express.json());

app.get("/getKalender", async (req, res) => {
  const CLIENT_ID =
    "337035628666-52umrq4ppns8q5jgeqv26ii5vun7l2sg.apps.googleusercontent.com";
  const API_KEY = "AIzaSyD1OFuhDJKBPbzN4qwoaDxRSA1l6CT7FV8";
  const DISCOVERY_DOCS = [
    "https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest",
  ];
  const SCOPES = "https://www.googleapis.com/auth/calendar.events";

  try {
    const { data } = await axios.post(
      "https://www.googleapis.com/auth/calendar.events/list",
      {
        calendarId: "primary",
        timeMin: new Date().toISOString(),
        showDeleted: false,
        singleEvents: true,
        maxResults: 10,
        orderBy: "startTime",
      },
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${YOUR_ACCESS_TOKEN}`, 
        },
      }
    );

    res.json(data);
  } catch (error) {
    console.error("Fehler beim Abrufen der Daten:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

app.listen(PORT, () => {
  console.log(`Server läuft auf Port ${PORT}`);
});
