import "./App.css";
import Weather from "./components/Weather";
import Time from "./components/Time";
import Clock from "./components/AnalogClock";
import Spotify from "./components/Spotify";
import "./time.css";
import "./weather.css";
import Modal from "react-modal";
import News from "./components/News";
import Kalender from "./components/Kalander";
import Draggable from "react-draggable";
import Drawing from "./components/Zeichnen";
import drawing from "../src/assets/drawing.png";
import React, { useState, useEffect } from "react";
Modal.setAppElement("#root");

function App() {
  const [positions, setPositions] = useState({});
  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleDrag = (component, position) => {
    setPositions((prevPositions) => ({
      ...prevPositions,
      [component]: position,
    }));
  };
  //Beim laden der Applikation wird die aktuelle Position aus dem localStorage geholt
  useEffect(() => {
    const fetchPositions = async () => {
      const savedPositions = JSON.parse(
        localStorage.getItem("componentPositions")
      );

      if (savedPositions) {
        setPositions(savedPositions);
      }
    };

    fetchPositions();
  }, []);
  //Speichert aktuelle Position der Komponenten
  const savePositions = () => {
    localStorage.setItem("componentPositions", JSON.stringify(positions));
  };
  // Holt die aktuelle Positionen der Komponenten
  const getComponentPosition = (component) =>
    positions[component] || { x: 0, y: 0 };

  return (
    <div className="App">
      <div>
        {/*Wetter Komponent einfügen */}
        <div className="weatherComponent">
          <Weather />
        </div>
      </div>
      {/*Draw Komponent einfügen */}
      <div className="drawingComponent">
        <img
          className="drawingIcon"
          src={drawing}
          style={{ cursor: "pointer" }}
          alt="drawingIcon"
          onDoubleClick={() => setIsModalOpen(true)}
        />

        <Modal
          isOpen={isModalOpen}
          onRequestClose={() => setIsModalOpen(false)}
          contentLabel="Drawing Modal"
          style={{
            overlay: {
              backgroundColor: "rgba(0, 0, 0, 0.7)",
            },
            content: {
              backgroundColor: "black",
              border: "2px solid white",
              borderRadius: "8px",
              padding: "20px",
              height: "600px",
              width: "900px",
            },
          }}
        >
          <Drawing />
        </Modal>
      </div>
      {/*Analoge und digitale-Uhr einfügen */}
      <Draggable
        onDrag={(e, data) => handleDrag("clock", { x: data.x, y: data.y })}
        position={getComponentPosition("clock")}
        onStop={() => savePositions()}
      >
        <div className="timeComponent">
          <Clock />
          <div className="digital">
            <Time />
          </div>
        </div>
      </Draggable>
      {/*Spotify-Komponent einfügen */}
      <Draggable
        onDrag={(e, data) => handleDrag("spotify", { x: data.x, y: data.y })}
        position={getComponentPosition("spotify")}
        onStop={() => savePositions()}
      >
        <div className="spotifyComponent">
          <Spotify />
        </div>
      </Draggable>
      {/*News-Komponent einfügen */}
      <Draggable
        onDrag={(e, data) => handleDrag("news", { x: data.x, y: data.y })}
        position={getComponentPosition("news")}
        onStop={() => savePositions()}
      >
        <div>
          <News />
        </div>
      </Draggable>
      {/*Kalender-Komponent einfügen */}
      <Draggable
        onDrag={(e, data) => handleDrag("kalender", { x: data.x, y: data.y })}
        position={getComponentPosition("kalender")}
        onStop={() => savePositions()}
      >
        <div>
          <Kalender />
        </div>
      </Draggable>
    </div>
  );
}
export default App;
