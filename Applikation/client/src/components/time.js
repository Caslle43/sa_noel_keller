import React, { useState, useEffect } from "react";
import "../time.css";

const Time = () => {
  const [currentTime, setCurrentTime] = useState(new Date());

  useEffect(() => {
    const updateTime = () => {
      setCurrentTime(new Date());
    };
    const intervalId = setInterval(updateTime, 1000);

    return () => clearInterval(intervalId);
  }, []);
// Datum richtig formatieren
  const formatTime = (date) => {
    const hours = date.getHours().toString().padStart(2, "0");
    const minutes = date.getMinutes().toString().padStart(2, "0");
    const seconds = date.getSeconds().toString().padStart(2, "0");

    return `${hours}:${minutes}:${seconds}`;
  };
// Wochentage definieren
  const formatDayAndDate = (date) => {
    const daysOfWeek = [
      "Sonntag",
      "Montag",
      "Dienstag",
      "Mittwoch",
      "Donnerstag",
      "Freitag",
      "Samstag",
    ];
    const dayOfWeek = daysOfWeek[date.getDay()];
    const day = date.getDate().toString().padStart(2, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const year = date.getFullYear();

    return `${dayOfWeek}, ${day}.${month}.${year}`;
  };

  return (
    <div className="clocks">
      <div
        className="digitalClock"
        style={{ color: "#ffffff", fontSize: "16px" }}
      >
        <p className="time">{formatTime(currentTime)}</p>
        <p className="dayAndDate">{formatDayAndDate(currentTime)}</p>
      </div>
    </div>
  );
};

export default Time;
