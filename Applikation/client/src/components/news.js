import React, { useEffect, useState } from "react";
import axios from "axios";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "../news.css";

const News = () => {
  const [news, setNews] = useState([]);
  const [isSliding, setIsSliding] = useState(false);
//API abfrage
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          "https://newsapi.org/v2/top-headlines?country=ch&apiKey=feae8e1dfec84d0bb4529e4ed1140aef"
        );
        setNews(response.data.articles.slice(0, 10));
        console.log(response);
      } catch (error) {
        console.error("Error:", error);
      }
    };

    fetchData();
  }, []);

  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    swipeToSlide: true,
    variableWidth: false,

    beforeChange: () => setIsSliding(true),
    afterChange: () => setIsSliding(false),
  };

  const handleDrag = () => {
    setIsSliding(false);
  };

  return (
    <div className="news-container">
      <Slider {...settings}>
        {news.map((article, index) => (
          <div key={index} className="news-item">
            <strong style={{ color: "#fff", fontSize: "14px" }}>
              {article.title}
            </strong>
            <p style={{ color: "#fff", fontSize: "12px" }}>{article.author}</p>
            <p style={{ color: "#fff", fontSize: "12px" }}>
              {article.publishedAt}
            </p>
          </div>
        ))}
      </Slider>
    </div>
  );
};

export default News;
