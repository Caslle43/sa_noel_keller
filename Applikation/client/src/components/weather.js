import React, { useState, useEffect } from "react";
import axios from "axios";
import "../weather.css";
import wind from "../assets/wind.png";
import luftfeuchtigkeit from "../assets/luftfeuchtigkeit.png";

const Weather = () => {
  const [wetter, setwetter] = useState({});
  const [stadt, setStadt] = useState("Frauenfeld");
  // Wetter API abfragen
  const getWeatherData = async () => {
    const url = `https://api.openweathermap.org/data/2.5/weather?q=${stadt}&appid=19a08d6161b1f385cbb418abafa65b0b`;

    try {
      const response = await axios.get(url);
      console.log(response);
      const temperatureCelsius = kelvinToCelsius(response.data.main.temp);
      //Wetterdaten dem useState zuweisen
      setwetter({
        temperature: temperatureCelsius,
        weatherDescription: response.data.weather[0].description,
        icon: response.data.weather[0].icon,
        luftfeuchtigkeit: response.data.main.humidity,
        windgeschwindigkeit: response.data.wind.speed,
      });
    } catch (error) {
      console.error("Fehler bei der API-Anfrage:", error);
    }
  };
  //Temperatur umrechnen
  const kelvinToCelsius = (kelvin) => {
    return (kelvin - 273.15).toFixed(2);
  };

  useEffect(() => {
    getWeatherData();
  }, [stadt]);

  return (
    <div className="weather container">
      <div className="weatherContainer">
        <div className="dropdown">
          <select
            className="form-select"
            id="myDropdown"
            onChange={(e) => setStadt(e.target.value)}
            value={stadt}
          >
            <option value="Zürich">Zürich</option>
            <option value="Frauenfeld">Frauenfeld</option>
            <option value="Bern">Bern</option>
            <option value="Luzern">Luzern</option>
            <option value="Miami">Miami</option>
          </select>
        </div>

        <div className="weatherInfo">
          <img
            className="weatherIcon"
            src={`http://openweathermap.org/img/w/${wetter.icon}.png`}
            alt="Wetter-Icon"
          />
          <p className="temperatur">{` ${wetter.temperature} °C`}</p>
          <p className="Beschreibung">{` ${wetter.weatherDescription}`}</p>
          <div className="windandluft">
            <div className="luftandIcon">
              <img
                className="luftIcon"
                src={luftfeuchtigkeit}
                alt="Wind-Icon"
              />
              <p className="Luftfeuchtigkeit">
                {` ${wetter.luftfeuchtigkeit}`} g/m3
              </p>
            </div>
            <div className="WindandIcon">
              <img className="windIcon" src={wind} alt="Wind-Icon" />
              <p className="Windgeschwindigkeit">
                {` ${wetter.windgeschwindigkeit}`} km/h
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Weather;
