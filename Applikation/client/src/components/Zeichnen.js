import React, { useRef, useEffect, useState } from "react";

const Drawing = () => {
  const canvasRef = useRef(null);
  const [context, setContext] = useState(null);
  const [drawing, setDrawing] = useState(false);

  useEffect(() => {
    const canvas = canvasRef.current;
    const ctx = canvas.getContext("2d");
    setContext(ctx);
  }, []);

  const startDrawing = (e) => {
    if (e.cancelable) {
      e.preventDefault();
    }

    setDrawing(true);
    draw(e);
  };
//Stoppen des zeichnen
  const stopDrawing = () => {
    setDrawing(false);
    if (context) {
      context.beginPath();
    }
  };
// Funktion fürs zeichnen
  const draw = (e) => {
    if (!drawing) return;

    const { clientX, clientY } = e.touches ? e.touches[0] : e;
    const rect = canvasRef.current.getBoundingClientRect();
    const offsetX = clientX - rect.left;
    const offsetY = clientY - rect.top;

    context.lineWidth = 5;
    context.lineCap = "round";
    context.strokeStyle = "#FFFFFF";
// Event für touch und mouse
    if (e.type === "mousedown" || e.type === "touchstart") {
      context.beginPath();
      context.moveTo(offsetX, offsetY);
    } else if (e.type === "mousemove" || e.type === "touchmove") {
      context.lineTo(offsetX, offsetY);
      context.stroke();
    }
  };
//Canvas Einstellungen
  return (
    <canvas
      ref={canvasRef}
      width={900}
      height={600}
      style={{ border: "2px white" }}
      onMouseDown={startDrawing}
      onMouseUp={stopDrawing}
      onMouseOut={stopDrawing}
      onMouseMove={draw}
      onTouchStart={startDrawing}
      onTouchEnd={stopDrawing}
      onTouchCancel={stopDrawing}
      onTouchMove={draw}
    />
  );
};

export default Drawing;
