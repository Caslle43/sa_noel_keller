import React from "react";

const Kalender = () => {
  /*
  const [events, setEvents] = useState([]);

  // Set your own client ID and API key
  const CLIENT_ID =
    "337035628666-52umrq4ppns8q5jgeqv26ii5vun7l2sg.apps.googleusercontent.com";
  const API_KEY = "AIzaSyD1OFuhDJKBPbzN4qwoaDxRSA1l6CT7FV8";

  let tokenClient;
  let gapiInited = false;
  let gisInited = false;
  const handleAuthClick = () => {
    if (window.gapi.client.getToken() === null) {
      // Prompt the user to select a Google Account and ask for consent to share their data
      // when establishing a new session.
      tokenClient.requestAccessToken({ prompt: "consent" });
    } else {
      // Skip display of account chooser and consent dialog for an existing session.
      tokenClient.requestAccessToken({ prompt: "" });
    }
  };

  // Sign out the user upon button click.
  const handleSignoutClick = () => {
    const token = window.gapi.client.getToken();
    if (token !== null) {
      window.google.accounts.oauth2.revoke(token.access_token);
      window.gapi.client.setToken("");
      document.getElementById("content").innerText = "";
    }
  };

  useEffect(() => {
    // Callback after api.js is loaded.
    const gapiLoaded = () => {
      window.gapi.load("client", initializeGapiClient);
    };

    // Callback after the API client is loaded. Loads the
    // discovery doc to initialize the API.
    const initializeGapiClient = async () => {
      await window.gapi.client.init({
        apiKey: API_KEY,
        discoveryDocs: [
          "https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest",
        ],
      });
      gapiInited = true;
      maybeEnableButtons();
    };

    // Callback after Google Identity Services are loaded.
    const gisLoaded = () => {
      tokenClient = window.google.accounts.oauth2.initTokenClient({
        client_id: CLIENT_ID,
        scope: "https://www.googleapis.com/auth/calendar.readonly",
        callback: "", // defined later
      });
      gisInited = true;
      maybeEnableButtons();
    };

    // Enables user interaction after all libraries are loaded.
    const maybeEnableButtons = () => {
      if (gapiInited && gisInited) {
        document.getElementById("authorize_button").style.visibility =
          "visible";
      }
    };

    // Callback after the Google Identity Services token is initialized.
    tokenClient.callback = async (resp) => {
      if (resp.error !== undefined) {
        throw resp;
      }
      await listUpcomingEvents();
    };

    // Sign in the user upon button click.

    // Print the summary and start datetime/date of the next ten events in
    // the authorized user's calendar.
    const listUpcomingEvents = async () => {
      let response;
      try {
        const request = {
          calendarId: "primary",
          timeMin: new Date().toISOString(),
          showDeleted: false,
          singleEvents: true,
          maxResults: 10,
          orderBy: "startTime",
        };
        response = await window.gapi.client.calendar.events.list(request);
      } catch (err) {
        document.getElementById("content").innerText = err.message;
        return;
      }

      const fetchedEvents = response.result.items;
      setEvents(fetchedEvents);
    };

    // Load Google API and Google Identity Services scripts
    const gapiScript = document.createElement("script");
    gapiScript.src = "https://apis.google.com/js/api.js";
    gapiScript.async = true;
    gapiScript.defer = true;
    gapiScript.onload = gapiLoaded;

    const gisScript = document.createElement("script");
    gisScript.src = "https://accounts.google.com/gsi/client";
    gisScript.async = true;
    gisScript.defer = true;
    gisScript.onload = gisLoaded;

    document.body.appendChild(gapiScript);
    document.body.appendChild(gisScript);

    return () => {
      document.body.removeChild(gapiScript);
      document.body.removeChild(gisScript);
    };
  }, []);
*/
  return (
    <div>
      <iframe
        src="https://outlook.office365.com/owa/calendar/e7f4481fc3da491d97fd7c8f6a36f6ca@stud.kftg.ch/f83d525d9a5448c096c23660afe5d94111469318069789077697/calendar.html"
        width="500"
        height="300"
        frameBorder="0"
        color="black"
        title="calender"
        allow="fullscreen"
      ></iframe>
    </div>
  );
};

export default Kalender;
