import React from "react";
import { render, screen, fireEvent, waitFor } from "@testing-library/react";
import "@testing-library/jest-dom";
import axios from "axios";
import Weather from "./Weather";

jest.mock("axios");
//Test für Wetterkomponent dropdown
describe("Weather component", () => {
  test("fetches and displays weather data for selected city", async () => {
    const mockWeatherData = {
      main: {
        temp: 293.15,
        humidity: 60,
      },
      weather: [{ description: "Clear", icon: "01d" }],
      wind: { speed: 10 },
    };

    axios.get.mockResolvedValue({ data: mockWeatherData });

    render(<Weather />);

    await waitFor(() => {
      expect(screen.getByText(" Frauenfeld °C")).toBeInTheDocument();
      expect(screen.getByText(" Clear")).toBeInTheDocument();
      expect(screen.getByText(" 60 g/m3")).toBeInTheDocument();
      expect(screen.getByText(" 10 km/h")).toBeInTheDocument();
    });

    fireEvent.change(screen.getByRole("combobox"), {
      target: { value: "Zürich" },
    });

    await waitFor(() => {
      expect(screen.getByText(" Zürich °C")).toBeInTheDocument();
    });
  });
});
